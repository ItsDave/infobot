<?php
/**
 * Created by PhpStorm.
 * User: davep
 * Date: 23/06/2016
 * Time: 20:01
 */

namespace App;


class Input
{

    private $regex =  '/^(?:[:](\S+) )?(\S+)(?: (?!:)(.+?))?(?: [:](.+))?$/';
    private $user_regex = '/^(\S+)!/';
    private $raw;

    private $systemCommands= ['PING', 'JOIN', 'QUIT', 'KICK'];

    private $host;
    private $command;
    private $channel;
    private $user;
    private $text;
    private $user_command;
    private $data;
    private $user_data;
    private $users;


    public function __construct($data, User $user)
    {
        if ($data == '') return;

        // Pass in the array of seen users;
        $this->users = $user;

        $this->raw = $data;

	var_dump($data);

        if (preg_match($this->regex, $data, $request)) {

            $this->raw      = isset($request[0])    ? trim($request[0]) : '';
            $this->host     = isset($request[1])    ? trim($request[1]) : '';
            $this->command  = isset($request[2])    ? trim($request[2]) : '';
            $this->channel  = isset($request[3])    ? trim($request[3]) : '';
            $this->data     = isset($request[4])    ? trim($request[4]) : '';

            echo $this->raw . "\r\n";

            $parts = explode(' ', $this->data); // Split the request down into words

            $command = str_replace(env('IRC_PREFIX'), '',
                trim(array_shift($parts))); // Remove the command out of the array and remove the prefix

            $this->user_command = $command;

            $this->user_data = $parts;

            $this->processUser();

            $this->logText();
        }

        return $request;
    }

    public function userCommand()
    {
        return $this->user_command;
    }

    public function userData()
    {
        return $this->user_data;
    }

    public function getData()
    {
        return $this->data;
    }

    public function getCommand()
    {
        return $this->command;
    }

    public function getUser()
    {
        return $this->user;
    }

    public function getChannel()
    {
        return $this->channel;
    }

    public function isSystem()
    {
        return (bool) in_array($this->command, $this->systemCommands);
    }

    protected function processUser()
    {
        preg_match($this->user_regex, $this->host, $user);

        if (isset($user[1])) {
            $this->user = trim($user[1]);

            $this->users->user($this->user);
        }
    }

    protected function logText()
    {

        // Ok, time for some quick checks
        if (! $this->channel) return;

        if (! $this->user) return;

        if (in_array($this->user, ['InfoBotV2', 'Hannah', 'Carol'])) return;

        $log = new Log();
        $log->message   = $this->data;
        $log->channel   = $this->channel;
        $log->user      = $this->user;

        $log->save();
    }
}
