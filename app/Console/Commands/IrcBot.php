<?php
namespace App\Console\Commands;

use App\Input;
use App\IrcServer;
use App\Command as IrcCommand;
use App\Response;
use App\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;

class IrcBot extends Command {

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'ircbot';
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Serve the application on the PHP development server";

    protected $server;
    protected $response;
    protected $command;

    /**
     * Execute the console command.
     *
     * @param IrcServer $server
     * @param IrcCommand|\App\Console\Commands\Command $command
     * @param Response $response
     */
    public function fire(IrcServer $server,  IrcCommand $command, Response $response)
    {

        $this->server = $server;
        $this->response = $response;
        $this->command = $command;

        $this->info(env('IRC_NICK') . ' is starting up.....');

        $host = $server->host . ':' . $server->port;

        $this->info('Attempting to connect to server: ' . $host);

        // Try to connect to the server. If it fails, abort
        if (!$server->connect()) {
            $this->error('Unable to connect to: ' . $host);
            die();
        }

        // Ok we are connected to the server - Log the user in.
        $this->info('Connected to: ' . $host);

        // Share the socket between modles
        $response->setSocket($server->socket);

        $this->info('Registering User: ' . $server->nick);
        $user = $command->user($server->nick);
        $response->send($user);

        $this->info('Registering Nick: ' . $server->nick);
        $user = $command->nick($server->nick);
        $response->send($user);

        $registered = 0;

        do {
            $data = stream_socket_recvfrom($this->server->socket, 4096);

            $user = new User;

            $input = new Input($data, $user);

            if ($input->isSystem()) {
                $cmd = $this->command->handleCommand($input);
                $this->response->send($cmd);
                $registered = 1;
            }

        } while (!feof($this->server->socket) && $registered == 0);

        $this->info('Joining Channel: ' . $server->channel);
        $chan = $command->join($server->channel);
        $response->send($chan);
 
        $chan = $command->join('#OffTopic');
        $response->send($chan);

        $this->processData();
    }

    /**
     *
     */
    private function processData()
    {
        $this->info('Starting Data Processing');

        $user = new User;

        do {
            $data = stream_socket_recvfrom($this->server->socket, 4096);

            if (empty($data)) {
                // Empty data set ignore.
                continue;
            }
            
            $input = new Input($data, $user);
            $this->command->set($input);
            $this->command->setUsers($user);

            // If it's a system command, work out what
            // we need to do with it.
            if ($input->isSystem()) {
                $cmd = $this->command->handleCommand($input);
                $this->response->send($cmd);
            }

            // Send some debug messages to the console.
    		$this->info('User:' . $input->getUser());
    		$this->info('Channel:' . $input->getChannel());
    		$this->info('Data:' . json_encode($input->getData()));
    		$this->info('User Data: ' . json_encode($input->userData()));

            // If the AI commands are loaded in, return a response.
            if (method_exists($this->command, 'thinky')) {
                if ($input->getChannel() == '#Pine64' && (stripos($input->getData(), 'infobot') !== false)) {
                    $this->info('Sending Bot Response');
                    $response = $this->command->think();
                    $this->response->send($response);
                }
            }

            if (null !== $input->getData() && stripos($input->getData(), env('IRC_PREFIX')) === 0) {

                $this->info('Command Received - ' . $input->userCommand());

                $command = $input->userCommand();

                if (method_exists($this->command, $input->userCommand())) {
                    $cmd =  $this->command->$command();
                    $this->response->send($cmd);
                } else {
                    $this->warn('INVALID COMMAND - ' . $input->userCommand());
                    $this->response->send($input->getUser(), 'Invalid command entered, enter !help for a list of valid commands');
                }
            }
		
		    if($input->getChannel() == 'InfoBotV2' && trim($input->getUser()) !== ''){
                //$this->info('Sending Bot Response - PM');
                //$response = $this->command->think(1);
               // $this->response->send($response);
            }

        } while (!feof($this->server->socket));
        	$this->error('REBOOTING INFOBOT');
        	Artisan::call('ircbot');
        	exit();
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [];
    }
}
