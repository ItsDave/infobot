<?php
/**
 * Created by PhpStorm.
 * User: davep
 * Date: 21/10/2016
 * Time: 20:44
 */

namespace App\Traits;

use Carbon\Carbon;
use App\Log;

trait SystemCommands
{

    /**
     * Throw an exception to force the bot to crash.
     * @return mixed
     * @throws Exception
     */
    public function crash() {
        throw new Exception ('Crash!');
    }


    public function flood ()
    {
        if (! $this->input->getUser()) return true;

        $rows = Log::where('user', $this->input->getUser())->where('created_at', '>', Carbon::now()->subSeconds(5))->count();

	echo 'FLOOD Check: '. $rows;

        if ($rows > 10) {
		echo 'KICKING USER';
            return $this->command('KICK', $this->input->getUser(), 'Flood Protection....SORRY..');
	}
    }


    /**
     * @return mixed
     */
    public function handleCommand($input)
    {
       var_dump('Here:' . $input->getCommand());

        switch ($input->getCommand()) {
            case 'PING':
                return $this->pong($input->getData());
                break;
            case 'KICK':
                echo 'BOT KICKED - REJOINING';
                return $this->join($input->getChannel());
            case 'JOIN':
                echo 'SENDING WELCOME';
                return $this->showWelcome();
                break;
        }
    }

    /**
     * @param $host
     * @return mixed
     */
    public function pong($host)
    {
        return sprintf('%s :%s', 'PONG', $host);
    }

    /**
     * @param null $input
     * @return mixed
     */
    public function join($input = null)
    {
        if (isset($input)){
            $output = $input;
        } else {
            $output = $this->input->userData();
            $output = $output[0];
        }

        return sprintf('%s %s', 'JOIN', $output);
    }

    /**
     *
     */
    public function shutdown()
    {
        echo 'Bot is shutting down...';
        exit();
    }

    /**
     * @return mixed
     */
    public function quit()
    {
        return sprintf('%s :%s', 'QUIT', 'Good bye world');
    }

    /**
     * @return mixed
     */
    public function part()
    {
        return sprintf('%s %s', 'PART', $this->input->getChannel());
    }

    /**
     * @param $channel
     * @param $msg
     * @return mixed
     */
    public function notice($channel, $msg)
    {
        return sprintf('%s %s :%s', 'NOTICE', $channel, $msg);
    }

    /**
     * @param null $input
     * @return mixed
     */
    public function nick($input = null)
    {
        if (isset($input)){
            $output = $input;
        } else {
            $output = $this->input->userData();
            $output = $output[0];
        }

        return sprintf('%s :%s', 'NICK',$output);
    }

    /**
     * @param $nick
     * @return mixed
     */
    public function user($nick)
    {
        return sprintf('%s %s %s %s :%s', 'USER', $nick, '8', '*', 'Bob');
    }

    /**
     * Stop the bot from responding to any commands.
     * @return bool
     */
    public function mute(){

        $data = $this->input->userData();

        $time = array_shift($data);

        $mutedUntil = \Carbon\Carbon::now();
        $mutedUntil = $mutedUntil->addSeconds($time * 60);

        $this->muted = $mutedUntil;

        if ((int) $time > 0){
            return $this->text($this->input->getChannel(), 'I\'m sorry i\'ve upset you, i\'ll be quiet for ' . $time . ' minutes.');
        }

        return $this->text($this->input->getChannel(), 'Thank you for giving me my voice back!');

    }

}
