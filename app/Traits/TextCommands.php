<?php
/**
 * Created by PhpStorm.
 * User: davep
 * Date: 21/10/2016
 * Time: 20:44
 */

namespace App\Traits;


use App\Message;

trait TextCommands
{
    /**
     * Send an ASCII heart, because why not?
     * @return bool
     */
    public function heart(){
        return $this->text($this->input->getChannel(), '•♥•♥•♥•♥•♥•'. implode(' ', $this->input->userData()) .'•♥•♥•♥•♥•♥');
    }

    public function pinebook()
    {
        return $this->text($this->input->getChannel(), "\x035PINEBOOK: THERE ARE CURRENTLY A FEW ISSUES WITH THE PINEBOOK, FOR INFORMATION AND FIXES, PLEASE SEE THIS THREAD: https://forum.pine64.org/showthread.php?tid=4472\x03");
    }

    /**
     * @return array
     */
    public function msg()
    {
        $output     = $this->input->userData();

        $method     = array_shift($output);
        $user       = array_shift($output);
        $string     = implode(' ', $output);

        switch ($method) {
            case 'add':
                $message            = new Message;
                $message->sender    = $this->input->getUser();
                $message->recpt     = $user;
                $message->message   = $string;
                $message->save();

                return $this->text($this->input->getChannel(), 'Message for ' . $user. ' saved!');
            case 'get':
                $msgs = Message::where('recpt', $this->input->getUser())->get();

                if (count($msgs) == 0) {
                    return $this->text($this->input->getUser(), 'Sorry, you have no messages');
                }

                $response[] = $this->text($this->input->getUser(), 'You have ' . count($msgs) . ' new messages:');

                foreach ($msgs as $msg) {
                    $response[] = $this->text($this->input->getUser(), $msg->sender . ': ' . $msg->message . ' sent at ' . $msg->created_at);
                }

                Message::where('recpt', $this->input->getUser())->delete();

                return $response;

            default:
                return $this->text($this->input->getChannel(), 'Invalid command, the available commands are add / get');
        }

    }

    /**
     * @return mixed
     */
    public function showWelcome()
    {
        $last_seen = $this->users->created_at($this->input->getUser());

        $fiveMins = \Carbon\Carbon::now();
        $fiveMins = $fiveMins->subSeconds(180);

        $msgs = Message::where('recpt', $this->input->getUser())->get();

        if (count($msgs) > 0) {
            return $this->text($this->input->getData(), 'Welcome back ' . $this->input->getUser() . ' you have ' . count($msgs) . ' messages. Please run "!msg get" to retrieve them.' );
        }

        if ( ! isset($last_seen) || $last_seen->gt($fiveMins)) {
            echo 'Sending Welcome Notice to: ' . $this->input->getUser();

            $response = [];

            $response[] = $this->text($this->input->getData(), 'Welcome ' . $this->input->getUser() . ', it looks like you\'re new here!');

            //$response[] = $this->text($this->input->getData(), "\x035PINEBOOK: THERE ARE CURRENTLY A FEW ISSUES WITH THE PINEBOOK, FOR INFORMATION AND FIXES, PLEASE SEE THIS THREAD: https://forum.pine64.org/showthread.php?tid=4472\x03");

            $response[] = $this->text($this->input->getData(), 'Make yourself at home, and feel free to ask any questions you have and join in with our random discussions!');
            $response[] = $this->text($this->input->getData(), 'For image downloads, use our mirror http://cdn.pine64.uk');
            $response[] = $this->text($this->input->getData(), 'This channel is recorded, you may find the answer to your question at our searchable archive http://irc.pine64.uk');

            return $response;
        }
    }

    /**
     * @return mixed
     */
    public function time()
    {
        return $this->text($this->input->getChannel(), 'The current time is: ' . date('d-m-Y H:i:s'));
    }

    /**
     * Repeat a string back as the bot's user
     */
    public function say()
    {
        $data = $this->input->userData();

        $channel = array_shift($data);
        $string = implode(' ', $data);

        return $this->text($channel, $string);
    }

    /**
     * @return array
     */
    public function help()
    {
        echo 'Send help' . "\r\n";

        $response = [];

        $response[] = $this->notice($this->input->getUser(), 'Please see a list of valid commands below:');
        $response[] = $this->notice($this->input->getUser(), '1) !help - Displays this screen');
        $response[] = $this->notice($this->input->getUser(), '2) !slap <user> <item (optional)> - Slap a fellow user');
        $response[] = $this->notice($this->input->getUser(), '3) !android - Show a list of the most common Android problems');
        $response[] = $this->notice($this->input->getUser(), '4) !remix - Show a list of the most common RemixOS problems');
        $response[] = $this->notice($this->input->getUser(), '5) !linux - Show a list of the most common Linux problems');
        $response[] = $this->notice($this->input->getUser(), '6) !welcome - Show basic instructions on how to set your username');
        $response[] = $this->notice($this->input->getUser(), '7) !cat - Show a random fact about our furry feline friends');
        $response[] = $this->notice($this->input->getUser(), '8) !trump - Show a random Trump quote');
        $response[] = $this->notice($this->input->getUser(), '9) !mama - Show a random Yo mama joke');
        $response[] = $this->notice($this->input->getUser(), '10) !chuck - Show a random Chuck Norris fact');

        return $response;
    }


}
