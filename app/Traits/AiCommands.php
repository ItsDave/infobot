<?php
/**
 * Created by PhpStorm.
 * User: davep
 * Date: 21/10/2016
 * Time: 20:44
 */

namespace App\Traits;


use ChatterBotApi\ChatterBotFactory;
use ChatterBotApi\ChatterBotThought;
use ChatterBotApi\ChatterBotType;

trait AiCommands
{
    /**
     * @param int $pm
     * @return mixed
     */
    public function think($pm = 0) {

        $string = $this->input->getData();

        echo 'Sending: ' . $string;

        $bot1 = ChatterBotFactory::create(ChatterBotType::PANDORABOTS, ChatterBotType::PANDORABOTS_DEFAULT_ID);
        $bot1session = $bot1->createSession();

        $string = str_ireplace('infobot', '', $string);
        $string = str_ireplace('infobotv2', '', $string);

        $th = ChatterBotThought::make($string);

        try {
            $resp = $bot1session->think($th->message());

            if ($pm == 1){
                return $this->text($this->input->getUser(), strip_tags($resp));
            }

            return $this->text($this->input->getChannel(), strip_tags($resp));
        } catch (IOException $e) {
            echo $e;
        } catch (Exception $e) {
            echo 'No Response';
            // Ignore these
        }

    }
}