<?php
/**
 * Created by PhpStorm.
 * User: davep
 * Date: 21/10/2016
 * Time: 20:44
 */

namespace App\Traits;


trait PineCommands
{
    public function android()
    {
        if (count($this->input->userData()) == 0) {
            $dest = $this->input->getChannel();
            $method = 'text';
        } else {
            $dest = $this->input->userData();
            $dest = $dest[0];
            $method = 'notice';
        }

        echo 'Send Android help' . "\r\n";

        $response = [];

        $response[] = $this->$method($dest, 'Having Android Troubles? See quick tips below:');
        $response[] = $this->$method($dest, '1) There are two types of Android OS images: DD and Phoenix Card');
        $response[] = $this->$method($dest, '2) All Linux images are available here: http://wiki.pine64.org/index.php/Pine_A64_Software_Release');
        $response[] = $this->$method($dest, '3) If using a dd image you will need a program called Etcher, or you can use DD at the command line if you are comfortable in doing so. For the Phoenix image you need a program called Phoenix card');
        $response[] = $this->$method($dest, '4) Make sure to download the right image for the program you intend to use. For DD images their size must match the size of your SD card');
        $response[] = $this->$method($dest, '5) Unzip the image and burn it using one of the methods outlined in the steps above');
        $response[] = $this->$method($dest, '6) Tip: on first boot disconnect ethernet, and connect it once the boot process has finished');
        $response[] = $this->$method($dest, '7) If you use Phoenix card, make sure you select  \'Startup!\' and leave the pine alone for 15 min after booting - background processes are active');
        $response[] = $this->$method($dest, '8) If you are using the official Pine64 touchscreen then there is a special image you need to download -  it is clearly marked as \'LCD PANEL RELEASE\' on wiki - you need to burn it using phoenix');

        return $response;
    }

    public function linux()
    {

        if (count($this->input->userData()) == 0) {
            $dest = $this->input->getChannel();
            $method = 'text';
        } else {
            $dest = $this->input->userData();
            $dest = $dest[0];
            $method = 'notice';
        }

        $response = [];

        echo 'Send Linux help' . "\r\n";

        $response[] = $this->$method($dest, 'Having Linux Troubles? See quick tips below:');
        $response[] = $this->$method($dest, '1) All Linux images are available here: http://wiki.pine64.org/index.php/Pine_A64_Software_Release');
        $response[] = $this->$method($dest, '2) Download Etcher, an opensource diskimager  - this is the program to burn dd images');
        $response[] = $this->$method($dest, '3) Unzip the Linux image then burn it to the SD card using Etcher or DD if you are happy doing so');
        $response[] = $this->$method($dest, '4) Once the image has burnt, boot the device and navigate to /usr/local/sbin');
        $response[] = $this->$method($dest, '5) `ls` will show you all the available scripts. To resize your card do `sudo ./resize_rootfs.sh`');
        $response[] = $this->$method($dest, '6) Tip: If you encounter any problems during boot disconnect all peripherals (ethernet, hat(s), USB2 keyboard) and try again.');

        return $response;
    }

    public function mac()
    {
        if (count($this->input->userData()) == 0) {
            $dest = $this->input->getChannel();
            $method = 'text';
        } else {
            $dest = $this->input->userData();
            $dest = $dest[0];
            $method = 'notice';
        }

        $response[] = $this->$method($dest, 'Have more then one Pine on the network? See below for help:');
        $response[] = $this->$method($dest, 'If you having issues with more then 1 pine64 unit on your network running the same OS img.');
        $response[] = $this->$method($dest, 'Please edit the /boot/uEnv.txt file and change the mac address line ( ethaddr=7a:6e:63:ac:21:f1  to  ethaddr= (or just delete the line!))');
        $response[] = $this->$method($dest, 'and either reboot the unit or run the pine64_eth0-mackeeper.sh script and reset the mac address.');

        return $response;
    }

    public function remix()
    {

        if (count($this->input->userData()) == 0) {
            $dest = $this->input->getChannel();
            $method = 'text';
        } else {
            $dest = $this->input->userData();
            $dest = $dest[0];
            $method = 'notice';
        }

        $response = [];

        echo 'Send Remixos help' . "\r\n";
        $response[] = $this->$method($dest, 'Having RemixOS Troubles? See quick tips below:');
        $response[] = $this->$method($dest, '1) There are two types of Remix OS images: DD and Phoenix Card');
        $response[] = $this->$method($dest, '2) All Linux images are available here: http://wiki.pine64.org/index.php/Pine_A64_Software_Release');
        $response[] = $this->$method($dest, '3) If using a dd image you will need a program called Etcher in order to burn it to the card. For the Phoenix image you need a program called Phoenix card');
        $response[] = $this->$method($dest, '4) Make sure to download the right image for the program you intend to use. For DD images their size must match the size of your SD card');
        $response[] = $this->$method($dest, '5) Unzip the image and burn it using one of the methods outlined in the steps above');
        $response[] = $this->$method($dest, '6) Tip: on first boot disconnect ethernet, and connect it once the boot process has finished');
        $response[] = $this->$method($dest, '7) If you use Phoenix card, make sure you select  \'Startup!\' and leave the pine alone for 15 min after booting - background processes are active');

        return $response;
    }
}