<?php
/**
 * Created by PhpStorm.
 * User: davep
 * Date: 21/10/2016
 * Time: 20:44
 */

namespace App\Traits;


trait FunnyCommands
{
    /**
     * @return mixed
     */
    public function cat()
    {
        $cat_url = 'http://catfacts-api.appspot.com/api/facts';
        $facts = file_get_contents($cat_url);
        $decoded = json_decode($facts);

        return $this->text($this->input->getChannel(), $decoded->facts[0]);
    }

    /**
     * @return mixed
     */
    public function chuck()
    {
        $api_url = 'https://api.chucknorris.io/jokes/random';
        $facts = file_get_contents($api_url);
        $decoded = json_decode($facts);

        return $this->text($this->input->getChannel(), $decoded->value);
    }

    /**
    * Random trump quote
    */
    public function trump()
    {
        if (count($this->input->userData()) == 0) {
            $api_url = 'https://api.whatdoestrumpthink.com/api/v1/quotes/random';
        } else{
            $api_url = 'https://api.whatdoestrumpthink.com/api/v1/quotes/personalized?q=' . implode(' ', $this->input->userData());
        }

        $facts = file_get_contents($api_url);
        $decoded = json_decode($facts);

        return $this->text($this->input->getChannel(), $decoded->message);
    }


    /**
     * @return mixed
     */
    public function mama()
    {
        $file = file('https://raw.githubusercontent.com/rdegges/yomomma-api/master/jokes.txt');
        $line = $file[array_rand($file)];

        return $this->text($this->input->getChannel(), $line);
    }

    /**
     * @return mixed
     */
    public function rats()
    {
        return $this->text($this->input->getChannel(), '*hugs* I\'m sorry things are not going to plan :( - InfoBot recommends a cat fact to cheer you up!' );
    }

    /**
     * @param Input $input
     * @return mixed
     */
    public function chocolate(Input $input) {
        echo 'SENDING chocolate';
        return $this->text($this->input->getChannel(), 'Remember Emily, a moment on the lips, is a lifetime on the hips!');
    }
}