<?php
/**
 * Created by PhpStorm.
 * User: davep
 * Date: 21/10/2016
 * Time: 20:44
 */

namespace App\Traits;

trait OpCommands
{

    /**
     * Validate the user is permitted to run op commands.
     * @return bool
     */
    private function validateUser()
    {
        if ( ! in_array($this->input->getUser(), ['ItsDave'])) {
            return false;
        }

        return true;
    }

    /**
     * Kick the specified user
     * !kick <user> <reason>
     * @return mixed
     */
    public function kick()
    {
        if (!$this->validateUser()) return $this->text($this->input->getChannel(), 'Sorry, you don\'t have permission to run that command');

        $output     = $this->input->userData();

        $user       = array_shift($output);
        $string     = implode(' ', $output);

        return $this->command('KICK', $user, $string);
    }

    /**
     *
     */
    public function ban()
    {

    }

    /**
     *
     */
    public function hop()
    {

    }

    /**
     *
     */
    public function aop()
    {

    }

    /**
     * Return IRC formatted strings.
     * @param String $who The person, or channel to message.
     * @param String $message The message to send
     * @return bool
     */
    private function command($command, $who, $message)
    {
        return sprintf('%s %s %s :%s', $command, $this->input->getChannel(), $who, $message);
    }
}