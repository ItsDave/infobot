<?php
/**
 * Created by PhpStorm.
 * User: davep
 * Date: 23/06/2016
 * Time: 19:50
 */

namespace App;

use App\Traits\AiCommands;
use App\Traits\FunnyCommands;
use App\Traits\OpCommands;
use App\Traits\PineCommands;
use App\Traits\SystemCommands;
use App\Traits\TextCommands;

class Command
{
    /**
     * Load in the required modules
     * 
     * Modules are located in the Traits folder
     */
    use SystemCommands;
    use PineCommands;
    use AiCommands;
    use OpCommands;
    use TextCommands;
    use FunnyCommands;

    protected $input;
    protected $server;
    protected $users;

    protected $muted;

    /**
     * Command constructor.
     * @param IrcServer $server
     */
    public function __construct(IrcServer $server) {
        $this->server = $server;
    }

    /**
     * @param User $user
     */
    public function setUsers(User $user)
    {
        $this->users = $user;
    }

    /**
     * Send a notification to the owners phone.
     * @return bool
     */
    public function fetch() {
        $file = file_get_contents('https://hooks.zapier.com/hooks/catch/762473/69ykax/?test=1');
        return $this->text($this->input->getChannel(), 'I have sent a notification to ItsDave\'s phone to inform him he is wanted.');
    }

    /**
     * Return IRC formatted strings.
     * @param String $who The person, or channel to message.
     * @param String $message The message to send
     * @return bool
     */
    private function text($who, $message)
    {
        if ($who == 'InfoBotV2') return false;

        $now = \Carbon\Carbon::now();

        if ($this->muted && $now->lt($this->muted)) {
            return false;
        } else {
            $this->muted = null;
        }

        return sprintf('%s %s :%s', 'PRIVMSG', $who, $message);
    }


    /**
     * @param Input $input
     */
    public function set(Input $input)
    {
        $this->input = $input;
    }

    /**
     * @return bool
     */
    public function seen()
    {
        $user = $this->input->userData();
        $user = $user[0];

        $last_seen = $this->users->seen($user);

        if ( ! isset($last_seen)){
            return $this->text($this->input->getChannel(), $user . ' has not been online since I have joined :(');
        }

        $date = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $last_seen);

        $dt =  $date->diffForHumans(\Carbon\Carbon::now());

        $dt = str_replace('before', 'ago', $dt);

        return $this->text($this->input->getChannel(), $user . ' was last seen ' . $dt);
    }

    /**
     * @return bool
     */
    public function activity()
    {
        $last_seen = $this->users->activity($this->input->getUser());

        $date = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $last_seen);

        $dt =  $date->diffForHumans(\Carbon\Carbon::now());

        $dt = str_replace('before', 'ago', $dt);

        return $this->text($this->input->getChannel(), 'There was last activity: ' . $dt);
    }

}
