<?php
/**
 * Created by PhpStorm.
 * User: davep
 * Date: 23/06/2016
 * Time: 19:49
 */

namespace App;
use Illuminate\Contracts\Logging\Log;

class Response
{

    protected $socket;

    public function setSocket($socket)
    {
        $this->socket = $socket;
    }

    public function send($output)
    {

	   if ($output === false) return;

        if (is_array($output)) {
            foreach($output as $item) {
                echo ('Sending:' . $item . "\r\n");
                fputs($this->socket, $item . "\r\n");
            }

            return;
        }

        echo ('Sending:' . $output . "\r\n");
        fputs($this->socket, $output . "\r\n");

    }


}
