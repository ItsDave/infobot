<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;

class User extends Model
{
    protected $users = [];

    public function user($in_user)
    {
        $user = User::where('nick', $in_user)->first();

        if ( ! is_object($user)) {
            $user = new User;
            $user->nick = $in_user;
            $user->save();
            return;
        }
    
        $user->touch();
        $user->save();
    }

    public function seen($user)
    {
        $user = User::where('nick', $user)->first();

        if ( ! is_object($user)) {
            return null;
        }

        return $user->updated_at;
    }


    public function activity($user)
    {
        $user = User::where('nick', '<>', $user)->orderBy('updated_at', 'DESC')->first();

        if ( ! is_object($user)) {
            return null;
        }

        return $user->updated_at;
    }

    public function created_at($user)
    {
        $user = User::where('nick', $user)->first();

        if ( ! is_object($user)) {
            return null;
        }

        return $user->created_at;
    }

}
