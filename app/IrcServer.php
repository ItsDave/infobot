<?php

namespace App;

class IrcServer
{

    protected $host;
    protected $nick;
    protected $channel;
    protected $password;
    protected $socket;

    public function __construct()
    {
        $this->host     = env('IRC_HOST');
        $this->port     = env('IRC_PORT');
        $this->nick     = env('IRC_NICK');
        $this->channel  = env('IRC_CHANNEL');
    }

    public function connect()
    {
        // Really should not have to silence this :(

	$context = [];
	$context['bindto'] = '0.0.0.0:0';
	 $context = array('socket' => $context);

        $this->socket = @stream_socket_client('tcp://' . $this->host . ':' . $this->port, $errno, $errstr, 30, STREAM_CLIENT_CONNECT, stream_context_create($context));

	   stream_set_blocking($this->socket, 1);

        return (bool) $this->socket;
    }


    public function disconnect()
    {
        fclose($this->socket);
    }

    public function __get($name)
    {
        return $this->$name;
    }
}
